import 'dart:convert';

List<Magodex> magodexFromJson(String str) => List<Magodex>.from(json.decode(str).map((x) => Magodex.fromJson(x)));

String magodexToJson(List<Magodex> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Magodex {
  static var length;

    Magodex({
        required this.name,
        required this.gender,
        required this.house,
        required this.dateOfBirth,
        required this.actor,
        required this.patronus,
        required this.image,
        required this.ancestry,
        required this.species,
        
    });

    final String name;
    final String house;
    final String gender;
    final String dateOfBirth;
    final String actor;
    final String patronus; 
    final String image;
    final String ancestry;
    final String species;
   


    factory Magodex.fromJson(Map<String, dynamic> json) => Magodex(
        name: json["name"],
        house: json["house"],
        gender: json["gender"],
        dateOfBirth: json["dateOfBirth"],
        actor: json["actor"],
        patronus: json["patronus"],
        image: json["image"],
        ancestry: json["ancestry"],
        species: json["species"],
        
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "house": house,
        "gender": gender,
        "dateOfBirth": dateOfBirth,
        "actor": actor,
        "patronus": patronus,
        "ancestry": ancestry,
        "species": species,
        
    };
    
}



import 'dart:html';

import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({super.key, this.magonombre, required this.color,  this.magocasa, this.magogender, this.magodatebirth, this.magopatronus,  this.magoancestry, this.magoactor, required this.herotag, this.magospecies, });

  final magonombre;
  final magogender;
  final magocasa;
  final magodatebirth;
  final magopatronus;
  final magoancestry;
  final magoactor;
  final magospecies;
  
  final Color color;
  final int herotag;
  
  

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {

    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: widget.color,
      body: Stack(
        alignment: Alignment.center,
        children: [
          Positioned(
            top: 20,
            left: 0,
            child: IconButton(onPressed: (){
             Navigator.pop(context);
            }, icon: Icon(Icons.arrow_back, color: Colors.white, size: 30,)),
          ),

        Positioned(
          top: 80,
          left: 5,
          child: Text(widget.magonombre, style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold,
            fontSize: 25 
          ),)),
          Positioned(
            bottom: 0,
            child: Container(
              width: width,
              height: height * 0.6,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30)),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(height: 30,),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        Container(
                          width: width * 0.3 ,
                          child: Text("Name", style: TextStyle(
                            color:Colors.blueGrey, fontSize: 18),)
                          ),
                       Container(
                          width: width * 0.3 ,
                          child: Text(widget.magonombre , style: TextStyle(
                            color:Colors.black, fontSize: 18 , fontWeight: FontWeight.bold),)
                          ),
                          
                          ]),
                    ),
                   Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        Container(
                          width: width * 0.3 ,
                          child: Text("Gender", style: TextStyle(
                            color:Colors.blueGrey, fontSize: 18),)
                          ),
                       Container(
                          width: width * 0.3 ,
                          child: Text(widget.magogender , style: TextStyle(
                            color:Colors.black, fontSize: 18 , fontWeight: FontWeight.bold),)
                          ),
                          
                          ]),
                    ),
                     Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        Container(
                          width: width * 0.3 ,
                          child: Text("Date Of Birth", style: TextStyle(
                            color:Colors.blueGrey, fontSize: 18),)
                          ),
                       Container(
                          width: width * 0.3 ,
                          child: Text(widget.magodatebirth , style: TextStyle(
                            color:Colors.black, fontSize: 18 , fontWeight: FontWeight.bold),)
                          ),
                          
                          ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        Container(
                          width: width * 0.3 ,
                          child: Text("Patronus", style: TextStyle(
                            color:Colors.blueGrey, fontSize: 18),)
                          ),
                       Container(
                          width: width * 0.3 ,
                          child: Text(widget.magopatronus , style: TextStyle(
                            color:Colors.black, fontSize: 18 , fontWeight: FontWeight.bold),)
                          ),
                          
                          ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        Container(
                          width: width * 0.3 ,
                          child: Text("Ancestry", style: TextStyle(
                            color:Colors.blueGrey, fontSize: 18),)
                          ),
                       Container(
                          width: width * 0.3 ,
                          child: Text(widget.magoancestry , style: TextStyle(
                            color:Colors.black, fontSize: 18 , fontWeight: FontWeight.bold),)
                          ),
                          
                          ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        Container(
                          width: width * 0.3 ,
                          child: Text("Actor", style: TextStyle(
                            color:Colors.blueGrey, fontSize: 18),)
                          ),
                       Container(
                          width: width * 0.3 ,
                          child: Text(widget.magoactor , style: TextStyle(
                            color:Colors.black, fontSize: 18 , fontWeight: FontWeight.bold),)
                          ),
                          
                          ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        Container(
                          width: width * 0.3 ,
                          child: Text("Species", style: TextStyle(
                            color:Colors.blueGrey, fontSize: 18),)
                          ),
                       Container(
                          width: width * 0.3 ,
                          child: Text(widget.magospecies , style: TextStyle(
                            color:Colors.black, fontSize: 18 , fontWeight: FontWeight.bold),)
                          ),
                          
                          ]),
                    ),

                  ],
                ),
              ),
              
              
            ),
          ),
          Positioned(
            top: 120,
            left: 30,
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(left:8.0, right: 8.0, top:4.0, bottom: 4.0),
                child: Text(widget.magocasa ,style: TextStyle(
                  color: Colors.white, 
                  
                ) ,),
              ),
              
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10),),
                color: Colors.black26
              ),
            )),
         Positioned
         (
          top: 30,
          right: 0,
          child: Image.asset('images/jaguares.png' , height: 250, fit: BoxFit.fitHeight,))
        ],
      ),
    );
  }
}
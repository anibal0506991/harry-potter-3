import 'dart:html';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:harry_flutter/model/Magodex.dart';
import 'package:harry_flutter/service/remote_servise.dart';
import 'package:harry_flutter/view/detail_screeen.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key, required String title});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  List<Magodex>? magos;
  var isLoaded = false;

  @override
  void initState() {
    super.initState();

    
    getData();
  }

  getData() async {

   magos = await RemoteService().getMagos();
   if(magos != null){
    setState(() {
      isLoaded = true;
    });
   }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 50,
            right: 10,
            child: Image.asset('images/jaguares.png' , height: 100, fit: BoxFit.fitHeight,)),
            Positioned(
              top: 80,
              left: 10,
              child: Text('Magodex',
              style: TextStyle(
                fontSize: 50, fontWeight: FontWeight.bold,color: Colors.black, fontFamily: 'HarryPotter'
              ),)),
          Positioned(
            top: 150,
            bottom: 0,
            width: width,
            child: Column(
            children: [
             Expanded(
                child: GridView.builder(gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 1.4,
                ), itemCount: magos!.length,
                   itemBuilder: (context, index) {
                    var casa = magos![index].house;
                     return InkWell(
                       child: Padding(
                         padding: const EdgeInsets.symmetric(vertical:8.0, horizontal: 10),
                         child: Container(  
                          decoration: BoxDecoration(
                            color: casa == 'Gryffindor' ? Color.fromARGB(255, 202, 30, 30) : casa == 'Slytherin' ? Color.fromARGB(255, 31, 143, 31) : casa == 'Hufflepuff' ? Color.fromARGB(255, 246, 250, 7) : casa == 'Ravenclaw' ? Color.fromARGB(255, 33, 99, 212) : Colors.grey,
                            borderRadius: BorderRadius.all(Radius.circular(20))
                          ),
                          
                          child: Stack(
                            children: [
                              
                              
                                Positioned(
                                  top: 30,
                                  left: 20,
                                  child: Text(
                                    magos![index].name, 
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 18,color: Colors.white, 
                                    ),
                                  ),
                                ),
                               Positioned(
                                  top: 55,
                                  left: 20,
                                  child: Container(
                                    
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:8.0 ,right: 8.0, top: 4, bottom: 4),
                                      child: Text(
                                        magos![index].house, 
                                        style: TextStyle(
                                          color:Colors.white
                                        ) ,
                                      ),
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(20)),
                                      color: Colors.black26,
                                    ),
                                  ),
                                ),
                                
                              
                            
                            ]
                          ),
                         ),
                       ),
                       onTap: () {
                         //navegar a detalles
                         Navigator.push(context, MaterialPageRoute(builder: (_) => DetailScreen(
                          magonombre: magos![index].name,
                          magocasa: magos![index].house,
                          magogender: magos![index].gender,
                          magodatebirth: magos![index].dateOfBirth,
                          magoancestry: magos![index].ancestry,
                          magopatronus: magos![index].patronus,
                          magoactor: magos![index].actor,
                          magospecies: magos![index].species,
                         
                          
                          herotag: index,
                          color: casa == 'Gryffindor' ? Color.fromARGB(255, 202, 30, 30) : casa == 'Slytherin' ? Color.fromARGB(255, 31, 143, 31) : casa == 'Hufflepuff' ? Color.fromARGB(255, 246, 250, 7) : casa == 'Ravenclaw' ? Color.fromARGB(255, 33, 99, 212) : Colors.grey,
                         )));

                       },
                     );
                   },
                   ),
                   
                   ),
            ],
                  ),
          ),
        ]
      )
    );
  }
}
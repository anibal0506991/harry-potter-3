import 'package:harry_flutter/model/Magodex.dart';
import 'package:http/http.dart' as http;


class RemoteService{
  Future<List<Magodex>?> getMagos() async {

    var client = http.Client();


    var url = Uri.parse('https://hp-api.onrender.com/api/characters');
    var response = await client.get(url);

    if(response.statusCode == 200){
      var json = response.body;
      return magodexFromJson(json);
    }

  }
}
